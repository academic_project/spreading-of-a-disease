"""
Ce fichier simule la propagation d'une maladie en employant différentes méthodes:
    -En dicrétisant (temporellement) le système d'équa. diff. de l'énoncé, on peut le
    réécrire afin d'obtenir un schéma d'Euler implicite. Et puis à chaque étape, résoudre cette équation non-linéaire
    à l'aide de la méthode de newton. 1ére simulation: Euler implicite et méthode de Nexton
    
    -En dicrétisant (temporellement) le système d'équa. diff. de l'énoncé, on peut le
    réécrire afin d'obtenir un schéma d'Euler implicite. Et puis à chaque étape, résoudre cette équation non-linéaire
    à l'aide de la méthode du point fixe. 2éme simulation: Euler implicite et méthode du point fixe
    
    -Enfin, une dernière simulation avec la méthode Euler explicite. Cela nous permettrait d'utiliser qu'une méthode (donc qu'une boucle for)
    3ème simulation: Euler explicite
"""

import numpy as np
import copy
import matplotlib.pyplot as plt

# =============================================================================
# Partie qui contient notre version personnelle de la fonction inverse d'une matrice
# =============================================================================
def sousMatrice(m,i,j):
    """
    On enlève la ligne i et la colonne j de la matrice m 
    et on retourne cette matrice
    
    Parameters
    ----------
    m : matrice initiale
        
    i : indice de la ligne qu'on supprime de la matrice initiale m
    
    j : indice de la colonne qu'on supprime de la matrice initiale m
    
    Returns la sous-matrice  
    -------
    la variable lignes contient toutes les ligne sauf la i+1éme ligne (d'indice i)
    """
    return [lignes[:j] + lignes[j+1:] for lignes in (m[:i]+m[i+1:])]

def Determinant(m):
    """
    on calcule le déterminant de la matrice m

    Parameters
    ----------
    m : matrice initiale 

    Returns det(m)
    -------
    On calcule le déterminant de chaque sous-matrice
    en faisant une récursivité jusqu'à avoir une matrice 2x2 (qui est la condition d'arrêt)
    La variable det contient la somme des déteminants de chaque sous-matrice
    """
    if len(m) == 2:
        return m[0][0]*m[1][1]-m[0][1]*m[1][0]

    det = 0
    for n in range(len(m)):
        det += ((-1)**n)*m[0][n]*Determinant(sousMatrice(m,0,n))
    return det

def inverse(m):
    """
    On s'appuie sur la formule inv(M) = transposé(comatrice(M))/determinant(M)
    Les deux premières boucles for servent à calculer les commatrices
    Ensuite, on fait la transposée avec la fonction np.transpose()
    Enfin, On divise tout les coeff par le determinant

    Parameters
    ----------
    m : matrice initiale

    Returns m^(-1) (l'inverse de la matrice m)
    -------
    Une subtilité était de gérer la différence de typage entre array et list...
    La variable res contiendra la matrice inverse.
    """
    m=m.tolist()
    determinant=Determinant(m)
    if len(m)==2:
        return [[m[1][1]/determinant, -1*m[0][1]/determinant],[-1*m[1][0]/determinant, m[0][0]/determinant]]
    res = []
    #on resoud par ligne en faisait inv M = transposé(comatrice(M))/determinat(M) et comatrice de M
    for ligne in range(len(m)):
        rangée = []
        for colone in range(len(m)):
            minor = sousMatrice(m,ligne,colone)
            rangée.append(((-1)**(ligne+colone)) * Determinant(minor))
        res.append(rangée)
    res = np.array(res)
    res = np.transpose(res)
    res = res.tolist()
	#on divise par le determinant
    for l in range(len(res)):
        for c in range(len(res)):
            res[l][c] = res[l][c]/determinant
    return np.array(res)



# =============================================================================
# Définition des variables globales (paramètres, conditions initiales, méthodes ...)
# Ce sont les valeurs à faire varier 
# =============================================================================

N = 1000        #taille de la population
beta = 1.3      #nbre de rencontres d'un invidu en moyenne
delta =  0.2    #facteur de réduction de l'infectivité d'une personne aprés traitement
alpha =  0.1    #fraction d'invidus I sélectionnés pour traitement par unité de temps
gamma = 0.1     #taux de guérison d'individus I par unité de temps
eta =  0.1      #taux d'immunité/décés aprés traitement par unité de temps

S0 = 500
I0 = 500
T0 = 0
R0 = 0
y0 = np.array([[S0],
               [I0],
               [T0],
               [R0]])

duree = 100             #période de simulation : Intervalle de jours [0,duree]
n = 100                 #le nombre de subdivisions de l'intervalle [0,durée] -> nombre d'itérations de notre boucle d'euler implicite/explicite
deltaT = (duree-0)/n    #ceci sera notre pas temporelle (en jours)

eps=1e-12   #epsilon de la méthode de newton/point fixe
Nmax=15     #nombre de répétitions pour la méthode de newton/point fixe

# =============================================================================
# Fonctions globales statiques
# =============================================================================

def f_euler(x):
    """
    Il s'agit de la fonction présent dans la relation de la méthode euler implicite/explicite:
    Xn+1 = Xn + deltaT * f(Xn+1) ou Xn+1 = Xn + deltaT * f(Xn)
    Fonction définie dans R^4 à valeurs dans R^4
    où les composantes de son image correspond aux données de l'énoncé 
    Paramètre x=(S(t),I(t),T(t),R(t))
    """
    y=np.zeros((4,1))
    y[0]=(-beta/N)*x[0]*x[1]+(-beta/N)*x[0]*delta*x[2]
    y[1]=(beta/N)*x[0]*x[1]+(beta/N)*x[0]*delta*x[2]-(alpha*x[1])-(gamma*x[1])
    y[2]=alpha*x[1]-(eta*x[2])
    y[3]=gamma*x[1]+eta*x[2]
    return y


def f_newton(x,Xn):
    """
    Il s'agit de la fonction présent dans la relation de récurrence de la méthode de Newton:
    Xn+1 = Xn + inv(Jf(Xn))*f(Xn)
    Cette fonction dépend des valeurs Xn=(Sn,In,Tn,Rn) calculé à l'étape précédente
    d'où la présence d'un deuxième paramètre, celui-ci changera à chaque étape
    On obtient cette relation en passant tous les membres à gauche dans l'équation d'euler implicite
    Paramètre x=(Sn+1,In+1,Tn+1,Rn+1) (ce sera notre inconnu Xn+1 à chaque étape n)
    Paramètre Xn=(Sn,In,Tn,R) (ce sera le Xn calculé à l'étape n-1)
    """
    y=np.zeros((4,1))
    y[0]=x[0]-Xn[0]+(beta/N)*x[0]*x[1]*deltaT+(beta/N)*x[0]*delta*x[2]*deltaT
    y[1]=x[1]-Xn[1]+(-beta/N)*x[0]*x[1]*deltaT+(-beta/N)*x[0]*delta*x[2]*deltaT+alpha*x[1]*deltaT+gamma*x[1]*deltaT
    y[2]=x[2]-Xn[2]-alpha*x[1]*deltaT+eta*x[2]*deltaT
    y[3]=x[3]-Xn[3]-gamma*x[1]*deltaT-eta*x[2]*deltaT
    return y


def df_newton(x):
    """
    Il s'agit de la jacobienne de la fonction présente dans la relation de récurrence de la méthode de Newton:
    Xn+1 = Xn + inv(Jf(Xn))*f(Xn)
    Paramètre x=(Sn,In,Tn,R) (ce sera le Xn calculé à l'étape n-1)
    """
    y=np.zeros((4,4))
    y[0,0]=1+(beta/N)*x[1]*deltaT+(beta/N)*delta*x[2]*deltaT
    y[0,1]=(beta/N)*x[0]*deltaT
    y[0,2]=(beta/N)*x[0]*deltaT*delta
    y[1,0]=(-beta/N)*x[1]*deltaT+(-beta/N)*delta*x[2]*deltaT
    y[1,1]=1-(beta/N)*x[0]*deltaT+alpha*deltaT+gamma*deltaT
    y[1,2]=(-beta/N)*x[0]*deltaT*delta
    y[2,1]=-alpha*deltaT
    y[2,2]=1+eta*deltaT
    y[3,1]=-gamma*deltaT
    y[3,2]=-eta*deltaT
    y[3,3]=1
    return y


def F_pointFixe(x,t):
    """
    Il s'agit de la fonction présent dans la relation de la méthode du point fixe:
    F(Xn)=Xn+1 où F est égale à  f(Xn) + Xn (avec f la fonction ci-dessus)
    Cette fonction dépend des valeurs Xn=(Sn,In,Tn,Rn) calculé à l'étape précédente
    d'où la présence d'un deuxième paramètre, celui-ci changera à chaque étape
    Paramètre x=(Sn+1,In+1,Tn+1,Rn+1) (ce sera notre inconnu Xn+1 à chaque étape n)
    Paramètre t=(Sn,In,Tn,R) (ce sera le Xn calculé à l'étape n-1)
    """
    return t+deltaT*f_euler(x)
    
# =============================================================================
# Programme principale contenant les trois simulations 
# =============================================================================


def main1():
    """
    Simulation avec Euler implicite et Newton
    
    Returns
    -------
    res : matrice 4x(n+1) car chaque ligne correspond aux valeurs S I T R et chaque colonne sont les Xn=(Sn,In,Tn,Rn) calculés
    """
    #matrice 4x(n+1) car n itérations + la CI
    res = np.array(y0)
    #t stockera le résultat calculé par euler implicite Xn+1=(Sn+1,In+1,Tn+1,Rn+1), à chaque étape n
    t=y0    #t est initialisé à la CI (condition initiale)
    for i in range(1,n+1): #boucle pour la relation euler implicite : période de simulation sur n jours
        
        #d'abord on calcule Xn+1=(Sn+1,In+1,Tn+1,Rn+1) avec newton
        #le x0 de newton est à chaque fois le dernier Xn calculé par euler implicite (ici en l'occurence t)
        x=t
        k=0
        erreur=1.0
        while(erreur>eps and k<Nmax):
            xold=copy.deepcopy(x)
            #le x calculé ci-dessous est une première valeur de Xn+1=(Sn+1,In+1,Tn+1,Rn+1)) par la méthode de newton
            x=x-np.dot(inverse(df_newton(x)),f_newton(x,t))   #décommenter cette ligne si vous voulez utiliser notre méthode inverse
            #x=x-np.dot(np.linalg.inv(df_newton(x)),f_newton(x,t))
            erreur=np.linalg.norm(x-xold)/np.linalg.norm(x)
            k=k+1
        
        #ici, la méthode de newton nous a donné une première valeur de Xn+1=(Sn+1,In+1,Tn+1,Rn+1) stockée dans x
        #on pourrait s'arrêter et passer à l'étape suivante (jour suivant) mais on injecte cette valeur dans la relation de
        #Euler implicite pour avoir une meilleure précision
        #Xn+1 = Xn + deltaT * f(Xn+1)
        t = t + deltaT * f_euler(x) #on calcule Xi+1 avec euler implicite, ici x est la valeur retournée par la méthode de newton
        
        res = np.concatenate((res,t),1)#on ajoute t au vecteur résultat

        
    return res #chaque ligne correspond aux valeurs S I T R, et les colonnes sont les Xn=(Sn,In,Tn,Rn) calculés

def main2():
    """
    Simulation avec Euler explicite
    
    Returns
    -------
    res : matrice 4x(n+1) car chaque ligne correspond aux valeurs S I T R et chaque colonne sont les Xn=(Sn,In,Tn,Rn) calculés

    """
    res = np.array(y0)
    t=y0
    for i in range(0,duree):
        #Xn+1 = Xn + deltaT * f(Xn)
        t=t+deltaT*f_euler(t) #On calcule Xn+1 à partir de Xn
        res = np.concatenate((res,t),1)#on ajoute t au vecteur résultat
    return res

def main3():
    """
    Simulation avec Euler implicite et Point fixe
    
    Returns
    -------
    res : matrice 4x(n+1) car chaque ligne correspond aux valeurs S I T R et chaque colonne sont les Xn=(Sn,In,Tn,Rn) calculés

    """
    #matrice 4x(n+1) car n itérations + la CI
    res = np.array(y0)  
    #t stockera le résultat calculé par euler implicite Xn+1=(Sn+1,In+1,Tn+1,Rn+1), à chaque étape n
    t=y0                #ainsi t commence à la CI
    for i in range(1,n+1): #boucle pour la relation euler implicite : période de simulation temporelle
        
        #On est à l'étape n;
        #d'abord on calcule Xn+1=(Sn+1,In+1,Tn+1,Rn+1) avec la méthode du point fixe
        x=t #le CI (x0) de la méthode du point fixe sera à chaque fois le dernier Xn-1 calculé par euler implicite (ici en l'occurence t)
        k=0
        erreur=abs(F_pointFixe(x,t)-x)
            
        while (erreur>eps).all() and k<Nmax:
            
            #le x calculé ci-dessous est une première valeur de Xn+1=(Sn+1,In+1,Tn+1,Rn+1) par la méthode du point fixe
            x=F_pointFixe(x,t)
            k=k+1
            erreur=abs(F_pointFixe(x,t)-x)
        
        #ici, la méthode du point fixe nous a donné une première valeur de Xn+1=(Sn+1,In+1,Tn+1,Rn+1) stockée dans x
        #on pourrait s'arrêter et passer à l'étape suivante mais on injecte cette valeur dans la relation de 
        #Euler implicite pour avoir une meilleure précision
              
        t = t + deltaT * f_euler(x) #on calcule Xi+1 avec euler implicite, ici x est la valeur retournée par la méthode du point fixe
        
        res = np.concatenate((res,t),1)   #on ajoute t au vecteur résultat

        
    return res # chaque ligne correspond aux valeurs S I T R , et les colonnes sont les Xn+1=(Sn+1,In+1,Tn+1,Rn+1) calculés

# =============================================================================
# Graphe représentant chaque variable S,I,R,T pour la méthode Euler implicite et Newton
# =============================================================================

v = main1()
temps = np.linspace(0,duree,n+1) #axe des abcisses
S=v[0,:]
I=v[1,:]
T=v[2,:]
R=v[3,:]

plt.plot(temps,S,'b',lw=3 , label='S')
plt.plot(temps,I,'y',lw=3 , label='I')
plt.plot(temps,R,'g',lw=3 , label='R')
plt.plot(temps,T,'r',lw=3 , label='T')
plt.legend()
plt.grid()
plt.xlabel('temps (jours)')
plt.ylabel('nombre de personnes')
plt.title('Simulation - Euler implicite/Newton')
plt.show()

# =============================================================================
# Graphe représentant chaque variable S,I,R,T pour la méthode Euler explicite
# =============================================================================

v=main2()
temps = np.linspace(0,duree,n+1) #axe des abcisses
S=v[0,:]
I=v[1,:]
T=v[2,:]
R=v[3,:]


plt.plot(temps,S,'b',lw=3 , label='S')
plt.plot(temps,I,'y',lw=3 , label='I')
plt.plot(temps,R,'g',lw=3 , label='R')
plt.plot(temps,T,'r',lw=3 , label='T')
plt.legend()
plt.grid()
plt.xlabel('temps (jours)')
plt.ylabel('nombre de personnes')
plt.title('Simulation - Euler explicite')
plt.show()

# =============================================================================
# Graphe représentant chaque variable S,I,R,T pour la méthode Euler implicite et Point fixe
# =============================================================================

v=main3()
temps = np.linspace(0,duree,n+1) #axe des abcisses
S=v[0,:]
I=v[1,:]
T=v[2,:]
R=v[3,:]


plt.plot(temps,S,'b',lw=3 , label='S')
plt.plot(temps,I,'y',lw=3 , label='I')
plt.plot(temps,R,'g',lw=3 , label='R')
plt.plot(temps,T,'r',lw=3 , label='T')
plt.legend()
plt.grid()
plt.xlabel('temps (jours)')
plt.ylabel('nombre de personnes')
plt.title('Simulation - Euler implicite/Point fixe')
plt.show()
